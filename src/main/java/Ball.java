import java.awt.*;
import java.awt.geom.Dimension2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.util.Random;

/*
    Ball Class represents ball objects on the screen
 */
public class Ball implements Runnable, Movable {
    /*************MEMBERS*************/
    private Ellipse2D.Double ball; // Ball objects shape attributes
    private Point2D.Double ballCenter; //Stores center of a ball (x,y is center of the ball). For collision detection purposes
    private Point2D.Double ballCoords; // Store location coords of a Ball (x,y is top left of ball). For drawing purposes
    private Point2D.Double ballVelocity; // Store x and y velocity of Ball
    private final Color colorOfBall; // Store randomized color of ball
    private double radiux;
    public static Random sRandom = new Random();
    private static final double CONSTANT_DENSITY = 1.0;
    private double ballMass;
    private static int threadRefresh = 20;


    /*****************CONSTRUCTOR******************/
    public Ball(Color ballColor, double xPos, double yPos)
    {
        //Set up coordinates, radius, color, position, center
        ballCoords = new Point2D.Double(xPos, yPos);
        radiux = sRandom.nextDouble()*20 + 15;
        colorOfBall = ballColor;
        setBallPosition( ballCoords, radiux);
        setBallCenter();

        /* Mass = density * volume. Density is constant, so mass is proportional to the volume.
         "2D" Volume is area. Area of a circle is pi* r^2. Radius is used in place of volume
        */
        ballMass = CONSTANT_DENSITY * getRadiux();

        // Set up the ball velocity components
        switch (sRandom.nextInt(4)){
            case 0:
                ballVelocity = new Point2D.Double(sRandom.nextInt(5), sRandom.nextInt(5));
                break;

            case 1:
                ballVelocity = new Point2D.Double(-sRandom.nextInt(5), sRandom.nextInt(5));
                break;

            case 2:
                ballVelocity = new Point2D.Double(sRandom.nextInt(5), -sRandom.nextInt(5));
                break;

            case 3:
                ballVelocity = new Point2D.Double(-sRandom.nextInt(5), -sRandom.nextInt(5));
                break;

        }

    } // End Ball constructor


    /**************METHODS***********/

    // private method to set initial ball object
    private void setBallPosition(Point2D loc, double radiux)
    {
        ball = new Ellipse2D.Double( loc.getX(), loc.getY(), radiux, radiux);
    }
    
    // private overloaded method to set new ball values
    private void setBallPosition(Point2D loc)
    {
        ball.setFrame(loc, new Dimension2D() {
            @Override
            public double getWidth() {
                return radiux * 2;
            }

            @Override
            public double getHeight() {
                return radiux * 2;
            }

            @Override
            public void setSize(double width, double height) {

            }
        });
    }

    // private method to return coordinates of ball object
    private Point2D.Double getBallCoords()
    {
        return ballCoords;
    }

    // private method to return color of ball object
    private Color getBallColor()
    {
        return colorOfBall;
    }

    // public method to return radius of ball object
    public double getRadiux() {
        return radiux;
    }

    // public method to get center of the ball object
    public Point2D.Double getBallCenter() { return ballCenter;    }

    // private method to set center of the ball object
    private void setBallCenter() {
        ballCenter = new Point2D.Double(ballCoords.getX() + getRadiux(), ballCoords.getY() + getRadiux());
    }

    // public method to get mass
    public double getBallMass() {return ballMass;   }

    // public method to change velocity after collision
    public void changeVelocity(double newXVel, double newYVel) {
        ballVelocity = new Point2D.Double(ballVelocity.getX() + newXVel, ballVelocity.getY() + newYVel);
    }


    // public method to get ball velocity (x,y)
    public Point2D.Double getBallVelocity() {return ballVelocity;}

    // public method to get the threadRefresh seconds
    public static int getThreadRefresh() {
        return threadRefresh;
    }

    // public method to set the threadRefresh seconds
    public static void setThreadRefresh(int msec) {threadRefresh = msec;}

    /***************INTERFACE METHODS*****************/

    // Runnable interface
    public void run()
    {
        while(true)
        {
            move();

            try
            {
                Thread.sleep(threadRefresh);
            }
            catch(Exception e){/*do nothing*/}
        }
    }

    //Movable interface
    public void move()
    {
        //Hits LEFT or RIGHT wall
        if (ballCenter.getX() - getRadiux() <= 0 || ballCenter.getX() + getRadiux() >= BallPanel.DIM.getWidth())
            ballVelocity = new Point2D.Double(-ballVelocity.getX(), ballVelocity.getY());
        //Hits TOP or BOTTOM wall
        else if (ballCenter.getY() - getRadiux() <= 0 || ballCenter.getY() + getRadiux() >= BallPanel.DIM.getHeight())
            ballVelocity = new Point2D.Double(ballVelocity.getX(), -ballVelocity.getY());

        // Set total velocity, location, position, center
        ballCoords.setLocation( ballCoords.getX()+ ballVelocity.getX(), ballCoords.getY() + ballVelocity.getY());
        setBallPosition(ballCoords);
        setBallCenter();
    }

    //Movable interface
    public void draw(Graphics2D g2d)
    {
        g2d.setPaint( this.getBallColor() );
        g2d.fillOval((int) this.getBallCoords().getX(), (int) this.getBallCoords().getY(), (int) this.getRadiux()*2, (int) this.getRadiux()*2 );
    }


} // end Ball class
