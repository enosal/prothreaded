import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.List;

/*
    BallPanel class composes the panel with the bouncing balls
 */
class BallPanel extends JPanel
{
    /*************MEMBERS*************/
    private static final int MAX_BALLS = 60;
    private List<Movable> balls = new ArrayList<>(MAX_BALLS); //make it large to avoid capacity issues.
    private static int numBalls = 4;
    public static Dimension DIM = new Dimension(1100, 600);
    ExecutorService ballThreadExecutor = Executors.newCachedThreadPool();
    final Random colorGenerator = new Random();
    final Random locGen = new Random();


    /**************** CONSTRUCTOR ***************/
    public BallPanel()
    {
        for (int i = 1; i <= MAX_BALLS; i++) {
            createBall();
        }

    } // End BouncingBalls constructor

    /*************** METHODS ***************/

    //Detects collisions
    public static boolean detectCollection(Ball ball1, Ball ball2) {
        //Radii calculation
        double combinedRadii = ball1.getRadiux() + ball2.getRadiux();

        //Distance between the center (distance formula)
        double xPart = ball1.getBallCenter().getX() - ball2.getBallCenter().getX();
        double yPart = ball1.getBallCenter().getY() - ball2.getBallCenter().getY();
        double distanceBtwnCntrs = Math.sqrt(Math.pow(xPart, 2) + Math.pow(yPart, 2));

        return combinedRadii >= distanceBtwnCntrs;
    }

    // Returns the number of balls that are bouncing
    public static int getNumBalls() {return numBalls;}

    // Sets the number of balls that are bouncing
    public static void setNumBalls(int num) {numBalls = num;    }

    // Returns the array of Movable balls
    public List<Movable> getBallList() {return balls;}

    // Creates a ball
    public void createBall() {
        //to prevent shades of black from showing up on black background
        Color ballColor = new Color(50 + colorGenerator.nextInt(205), 50 + colorGenerator.nextInt(205), 50 + colorGenerator.nextInt(205));
        //Range for starting x and y is between 1/4 and 3/4th of the DIM (in x and y each)
        Ball ball = new Ball(ballColor, (1/4.0) + (1/2.0) * locGen.nextInt((int) DIM.getHeight()), (1/4.0) + (1/2.0) * locGen.nextInt((int)DIM.getWidth()));
        //Add ball to list
        balls.add(ball);
        // Executes new thread of created ball object
        ballThreadExecutor.execute(ball);
    }

    // Get the max number of balls allowed
    public static int getMaxBalls() {return MAX_BALLS;}



    /******** OVERRIDE METHODS ********/

    // Dimensions of JPanel
    @Override
    public Dimension getPreferredSize()
    {
        return DIM;
    } // End Overloaded JPanel Dimension

    //Paints
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponents(g);
        Graphics2D g2d = (Graphics2D) g;
        for(int i = 0; i < numBalls; i++)
        {
            balls.get(i).draw(g2d);
        }

    } //end paintComponent

} // end BallPanel class