import javax.swing.*;
import java.awt.*;


/*
    BouncingBalls class is the driver for the project
    Original source from here: https://github.com/moldypeach/Multithreaded-Bouncing-Balls-GUI
 */
public class BouncingBalls
{

    public static void main(String[] args)
    {
        /********** BALL PANEL ************/
        BallPanel ballPanel = new BallPanel(); // Create BallPanel - JPanel Object
        JFrame jFrame = new JFrame("ProThreaded");
        jFrame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        jFrame.add( ballPanel );
        jFrame.setBackground(Color.BLACK);
        jFrame.pack();
        jFrame.setLocation(0, 0);
        jFrame.setVisible(true);
        jFrame.setResizable(false);

        /************ CONTROL PANEL ************/
        ControlPanel controlPanel = new ControlPanel();
        JFrame jFrameCntrl = new JFrame("Control");
        jFrameCntrl.add(controlPanel);
        jFrameCntrl.pack();
        jFrameCntrl.setResizable(false);
        jFrameCntrl.setLocation(jFrame.getWidth(), jFrame.getHeight() / 2);
        jFrameCntrl.setVisible(true);


        /************ COLLISION DETECTION THREAD ************/
        Runnable collisionDetection = () -> {
            while (true) {
                for (int i = 0; i < BallPanel.getNumBalls(); i++) {
                    for (int j = i + 1; j < BallPanel.getNumBalls(); j++) {
                        Ball ball1 = (Ball) ballPanel.getBallList().get(i);
                        Ball ball2 = (Ball) ballPanel.getBallList().get(j);
                        /* My initial code for ball collision handling
                        (using wikipedia's equations for elastic collisions in 2D with 2 moving objects)
                        resulted in the balls sticking together.
                        Solution adapted from: http://gamedev.stackexchange.com/questions/20516/ball-collisions-sticking-together
                        */
                        if (BallPanel.detectCollection(ball1, ball2)) {
                            double yDist = ball1.getBallCenter().getY() - ball2.getBallCenter().getY();
                            double xDist = ball1.getBallCenter().getX() - ball2.getBallCenter().getX();
                            double distSq = Math.pow(xDist, 2) + Math.pow(yDist, 2);
                            double xVelocity = ball2.getBallVelocity().getX() - ball1.getBallVelocity().getX();
                            double yVelocity = ball2.getBallVelocity().getY() - ball1.getBallVelocity().getY();
                            double dotProduct = xDist * xVelocity + yDist * yVelocity;
                            if (dotProduct > 0) {
                                double collisionScale = dotProduct / distSq;
                                double xCollision = xDist * collisionScale;
                                double yCollision = yDist * collisionScale;
                                double combinedMass = ball1.getBallMass() + ball2.getBallMass();
                                double collisionWeightA = 2 * ball2.getBallMass() / combinedMass;
                                double collisionWeightB = 2 * ball1.getBallMass() / combinedMass;
                                ball1.changeVelocity(collisionWeightA * xCollision, collisionWeightA * yCollision);
                                ball2.changeVelocity(-(collisionWeightB * xCollision), -(collisionWeightB * yCollision));
                            } //end dot product if
                        }// end collision detection if
                    } //end inner for
                } //end outer for
            } //end while
        };//end Runnable

        // start the collision detection thread
        new Thread(collisionDetection).start();


        /************ GUI CONTROL THREAD ************/
        Runnable guiControl = () -> {
            while (true) {
                if (controlPanel.getBallChangeStatus()) {
                    //sets the new number of balls in the ball panel from the control panel
                    BallPanel.setNumBalls(controlPanel.getGUIBalls());

                    //Slider change action is finished
                    controlPanel.setBallChangeStatus(false);
                } //end if
            }//end while
        };//end Runnable

        // start the collision detection thread
        new Thread(guiControl).start();

        /************ PAINTING ************/
        while(true)
        {
            jFrame.repaint(); // Repaint JFrame's black background
            ballPanel.repaint(); // Move each Ball object
            try
            {
                Thread.sleep(10);
            }
            catch(Exception event){/*do nothing*/}
        }

    } // End main


} // End BouncingBalls class

