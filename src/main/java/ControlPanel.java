import javax.swing.*;
import javax.swing.event.ChangeListener;
import java.awt.*;

/**
 * ControlPanel class composes the user interactive panel with the options to:
 * - increase/decrease the amount of balls
 *
 */
public class ControlPanel extends JPanel {
    /*************MEMBERS*************/
    private static JSlider ballSlider;
    private static JLabel ballText = new JLabel();
    private boolean ballsChanged = false;
    private static JSlider timeSlider;
    private static JLabel timeText = new JLabel();

    /*************CONSTRUCTOR*************/
    public ControlPanel()
    {
        //Set layout
        this.setLayout(new BorderLayout());

        //Ball panel
        Panel ballPanel = new Panel();
        createBallSlider();
        ballPanel.add(new JLabel("Number of Balls: "));
        ballPanel.add(ballSlider);
        ballPanel.add(ballText);
        ballPanel.add(new JLabel(" balls"));
        this.add(ballPanel, BorderLayout.NORTH);

        //Time panel
        Panel timePanel = new Panel();
        createTimeSlider();
        timePanel.add(new JLabel("Time between calls: "));
        timePanel.add(timeSlider);
        timePanel.add(timeText);
        timePanel.add(new JLabel(" msecs"));
        this.add(timePanel, BorderLayout.SOUTH);
    }

    /************* BALL SLIDER METHODS *************/

    // private method to create Ball slider
    private void createBallSlider() {
        final int BALL_MIN = 0;
        final int BALL_MAX = BallPanel.getMaxBalls();
        final int BALL_INIT = BallPanel.getNumBalls();

        //Slider & Text
        ballSlider = new JSlider(JSlider.HORIZONTAL,BALL_MIN, BALL_MAX, BALL_INIT);
        ballText.setText("" + BallPanel.getNumBalls());

        //Change listener
        ChangeListener listener = (e) -> {
            ballText.setText("" + ballSlider.getValue());
            setBallChangeStatus(true);
        };
        ballSlider.addChangeListener(listener);

        //Labels & Ticks for slider
        ballSlider.setMajorTickSpacing(10);
        ballSlider.setMinorTickSpacing(5);
        ballSlider.setPaintTicks(true);
        ballSlider.setPaintLabels(true);
    }

    //public method to change whether the ball slider has been active
    public void setBallChangeStatus(boolean change) {
        ballsChanged = change;
    }

    //public method to check whether the ball slider has been active
    public boolean getBallChangeStatus() {
        return ballsChanged;
    }

    //public method to get the specified number of balls (by user)
    public int getGUIBalls() {
        return ballSlider.getValue();
    }


    /********** DRAW/MOVE METHODS ************/

    //private method to create time slider
    private void createTimeSlider() {
        final int TIME_MIN = 10;
        final int TIME_MAX = 210;
        final int TIME_INIT = Ball.getThreadRefresh();

        //Slider & Text
        timeSlider = new JSlider(JSlider.HORIZONTAL,TIME_MIN, TIME_MAX, TIME_INIT);
        timeText.setText("" + Ball.getThreadRefresh());

        //Change listener
        ChangeListener listener = (e) -> {
            timeText.setText("" + timeSlider.getValue());
//            setBallChangeStatus(true);
            Ball.setThreadRefresh(timeSlider.getValue());
        };
        timeSlider.addChangeListener(listener);

        //Labels & Ticks for slider
        timeSlider.setMajorTickSpacing(100);
        timeSlider.setMinorTickSpacing(10);
        timeSlider.setPaintTicks(true);
        timeSlider.setPaintLabels(true);
    }

}
