import java.awt.*;

/**
 * Movable interface has two methods:
 * -move()
 * -draw(Graphics2d)
 */
public interface Movable {
    void move();
    void draw(Graphics2D g);
}
